﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Assignment2.Models;
using System.Linq.Dynamic;

namespace Assignment2.Controllers
{
    public class ClimateController : Controller
    {
        private BureauDBEntities db = new BureauDBEntities();

        public ActionResult Index()
        {
            try
            {
                var stationData = db.Stations.Select(c => new
                {

                    Code = c.stationNumber,
                    station = c.stationName

                });
                return View();



            }
            catch (Exception ex) {
                // do smothing with the exception
                throw ex;
            }
            } 

        public ActionResult Details()
        {
            return View();
        }
    }
}
